/*** estudiantes.h***/


#ifndef ESTUDIANTES_H
*** estudiantes.h***/


#ifndef ESTUDIANTES_H
#define ESTUDIANTES_H

typedef struct {
	//char codigo[6];
	//char nombre[10];
	float proy1, proy2, proy3;
	float cont1, cont2, cont3, cont4, cont5, cont6;
} asignatura; 


typedef struct{
	char nombre[10];
	char apellidoP[10];
	char apellidoM[10];
	asignatura asig_1; 
	float prom;
} estudiante;


void cargarEstudiantes(char path[], estudiante curso[]); 

void grabarEstudiantes(char path[], estudiante curso[]);
